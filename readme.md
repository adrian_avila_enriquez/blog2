# **Basic Blog** #

	This is mini project for learning to use Laravel Framework (v.4). The code implements basic functionality of a blog by managing properties of Models, Views, Controllers, Routes and Filters.


## **Pre-requisites.** ##

	You will need to install Composer, the dependency manager for PHP from [Composer Website](https://getcomposer.org/)


# **Installation** #

	1. Create a new folder inside your server directory:
	   (For example if your are using XAMPP) 

		$ cd /opt/lampp/htdocs
		$ sudo mkdir laravel_projects
		$ cd laravel_projects


	2. Initialize a repository in your new folder. Then clone this repo into it:

		$ sudo git init
		$ sudo clone https://adrian_avila_enriquez@bitbucket.org/adrian_avila_enriquez/blog1.git
		$ cd blog1


	3. Install and update composer:

		$ composer install
		$ composer update


	4. Change the permssions for the storage : Folders within storage require write access by the web server:

		$ cd app/storage
		$ sudo chmod 777 *


# **Execution** #

	Now you are ready. Try the application. Go to the browser and type: http://localhost/laravel_projects/blog1/public/

	Be sure that your virtual server has been started. If your are using XAMPP, type in the command line:

		$ sudo /opt/lampp/lampp start