<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Public Routes
Route::get('/', array('uses' => 'PostController@index'));

Route::resource('login', 'LoginController');

Route::resource('post', 'PostController');

Route::resource('user', 'UserController');

// Protected Routes
Route::group(['before' => 'auth'], function()
{		
	Route::resource('category', 'CategoryController');

	Route::resource('tag', 'TagController');

	Route::resource('administrator', 'AdministratorController');

	Route::get('/logout', array( 'uses' => 'LoginController@logOut',
		'as' => 'logout'));

});