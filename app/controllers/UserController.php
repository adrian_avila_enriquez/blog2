<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::where('rol', '=','0')
			->where('id', '!=','1')->get();

		// Redirects to blog - admin view - users
		return View::make('administrator.users')
				->with('users', $users);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('signup');	
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{	
		// Validation of new user data
		$validator = User::validate(Input::all());

		if ($validator->passes()){
			
			// Validation successfull
			User::create(array(
                'name' 		=> Input::get('name'),
                'username' 	=> Input::get('username'),
                'email' 	=> Input::get('email'),
                'password' 	=> Hash::make(Input::get('password'))
        	));

			return Redirect::route('login.index')
				->with('message', 'Your account has been created!');
		}
		else{		

			// Validation failed
			return Redirect::route('user.create')
				->withErrors($validator);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);
		$user->delete();

		// Redirects to blog - admin view - users
		return Redirect::route('user.index');
	}

}