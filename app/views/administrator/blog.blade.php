@extends('layouts.administrator')

@section('head')
@parent
@stop 

@section('navbar')
@parent
@stop

@section('content')
<!-- Page Heading -->
<div class="row">
	<br>
	<div class="col-lg-12" align="right">
		<h1 class="page-header">{{$user->name}}
			<small>administrator</small>
		</h1>
	</div>
</div>
<!-- /.row -->

<!-- Posts -->
@foreach($posts as $post)
<div class="row">
	<div class="col-md-7">
		<a href="#">
			<img class="img-responsive" src="http://res.cloudinary.com/azurlingua/image/upload/v1425572228/laravel_logo-700x300_c6mbdy.jpg" alt="">
		</a>
		<p>
			{{ Form::open(array('route' => array('post.update', $post->id), 'method' => 'PUT')) }}
			{{ Form::submit('Add tag?',  array('class' => 'btn btn-warning btn-sm')) }}		
			{{ Form::select('tags[]',$tag_options,null,array('multiple'=>'multiple'))}} 			
			{{ Form::close() }}
		</p>
	</div>
	<div class="col-md-5">
		<h2>{{$post->title}}</h2>
		<h4>By {{$post->user->username}}</h4>
		<h5>Posted on {{$post->created_at}}</h5>
		<p>Categories :
			@foreach($post->categories as $category)
			{{$category->name}}
			@endforeach
		</p>
		<p>Tagged as :
			@foreach($post->tags as $tag)
			{{$tag->description}}
			@endforeach
		</p>
		<p>
			{{ HTML::linkRoute('post.show', 'Inspect', $post->id, array('class' => 'btn btn-success ')) }}
		</p>
		
		<p>
			{{ Form::open(array('route' => array('post.destroy', $post->id), 'method' => 'DELETE')) }}
			{{ Form::submit('Delete', array('class' => 'btn btn-danger ', 'onClick' => "return confirm('Are you sure you want to delete this post ?')")) }}
			{{ Form::close() }}
		</p>
	</div>
</div>
<hr>
<!-- /.row -->
@endforeach 
<!-- /.container -->
@stop

@section('footer')
@parent
@stop

@section('script')
@parent
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/js/select2.min.js"></script>
<script type="text/javascript"> $('select').select2() </script>
@stop

