@extends('layouts.master')

@section('head')
@parent
@stop

@section('navbar')
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			{{ HTML::linkRoute('post.index', 'Blog', array(), array('class' => 'navbar-brand')) }}
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li>
					{{ HTML::linkRoute('login.index', 'Log in') }}

				</li>
				<li>
					{{ HTML::linkRoute('user.create', 'Sign up') }}
				</li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container -->
</nav>
@show

@section('footer')
@parent
@stop

@section('script')
@parent
@stop