<?php

class PostTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('posts')->delete();
        \DB::table('posts')->insert(array(
          array(
            'user_id'   => '4',
            'title'    => 'First post',
            'content'  => 'This is the first post: Nothing'
            ),
          array(
            'user_id'   => '3',
            'title'    => 'Second post',
            'content'  => 'This is the second post: Still nothing'
            ),
          array(
            'user_id'   => '2',
            'title'    => 'Third post',
            'content'  => 'This is the third post: Something'
            )
          ));
    }
}