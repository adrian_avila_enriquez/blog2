<?php

class UserTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('users')->delete();
        \DB::table('users')->insert(array(
          array(
            'name'     => 'Anonymous',
            'rol'      => '0',
            'username' => 'Anonymous',
            'email'    => 'anonymous@gmail.com',
            'password' => Hash::make('anonymous')
            ),
          array(
            'name'     => 'Adrian Avila',
            'rol'      => '1',
            'username' => 'adrian',
            'email'    => 'azurmedia.ad@gmail.com',
            'password' => Hash::make('avila')
            ),
          array(
            'name'     => 'Ludovic Floch',
            'rol'      => '0',
            'username' => 'ludovic',
            'email'    => 'azurmedia.lu@gmail.com',
            'password' => Hash::make('floch')
            ),
          array(
            'name'     => 'Yann Librati',
            'rol'      => '0',
            'username' => 'yann',
            'email'    => 'azurlingua@gmail.com',
            'password' => Hash::make('librati')
            ),
          array(
            'name'     => 'Patricia Camacho',
            'rol'      => '0',
            'username' => 'patricia',
            'email'    => 'azurmedia.pa@gmail.com',
            'password' => Hash::make('camacho')
            ),
          ));
}

}