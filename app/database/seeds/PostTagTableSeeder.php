<?php

class PostTagTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('post_tag')->delete();
        \DB::table('post_tag')->insert(array(
          array(
            'post_id'   => '1',
            'tag_id'   => '1'          
            ),
          array(
            'post_id'   => '2',
            'tag_id'   => '2' 
            ),
          array(
            'post_id'   => '3',
            'tag_id'   => '3' 
            )
          ));
    }
}