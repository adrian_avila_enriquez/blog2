<?php

class TagTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('tags')->delete();
        \DB::table('tags')->insert(array(
          array(
            'description'   => 'Pacific',
            ),
          array(
            'description'   => 'Moderate',
            ),
          array(
            'description'   => 'Aggressive',
            ),
          array(
            'description'   => 'Extra-aggressive',
            )
          ));
    }
}